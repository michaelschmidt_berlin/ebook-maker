const {
    asin,
    isbn,
    version,
    license,
    repository,
} = require('./package.json');

module.exports = {
    metadata: {
        title: '${BOOK_TITLE}',
        authors: '${AUTHOR}',
        outputProfile: 'kindle',
        bookProducer: 'ebook-maker',
        comments: license,
        isbn,
        pubdate: '<DATE>',
        publisher: '<Add an publisher>',
    },
    additionalMetadata: {
        asin,
        version,
        license,
        repository,
    },
    lookAndFeel: {
        extraCss: '${PROJECT_DIR}/media/global.css',
        embedAllFonts: true,
        lineHeight: 22,
        changeJustification: 'justify'
    },
    txtInputOptions: {
        formattingType: 'markdown'
    },
    mobiOutputOptions: {
        mobiFileType: 'both'
    },
    epubOutputOptions: {
    },
    azw3OutputOptions: {
        prettyPrint: true
    },
    pdfOutputOptions: {
        paperSize: 'a5',
        pdfPageNumbers: true,
        prettyPrint: true,
        pdfDefaultFontSize: 13,
        pdfAddToc: true,
        pdfHyphenate: true,
        pdfOddEvenOffset: 10,
        pdfPageMarginTop: 48.0,
        pdfPageMarginBottom: 48.0,
        pdfPageMarginLeft: 72.0,
        pdfPageMarginRight: 72.0
    },

    /**
     * 
     */
    tableOfContents: {
        tocTitle: 'Inhalt',
        level1Toc: '//h:h1',
        level2Toc: '//h:h2',
        // level3Toc: '//h:h3',
        useAutoToc: true,
    }
}
