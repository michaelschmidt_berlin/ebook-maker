import { VALID_EBOOK_TYPES } from '../common';

export type IAction = 'init' | 'create' | 'upload' | 'publish' | 'transcribe' | 'translate' | 'normalize';

export interface IGeneralBookProductionOptions {
    bookTypesToUpload: IEbookType[],
}

interface metadata {
    outputProfile: 'kindle',
    authors: string,
    bookProducer: string,
    comments: string,
    isbn: string,
    pubdate: string,
    publisher: string,
    title: string,
}

interface additionalMetadata {
    asin: string,
    version: string,
    license: string,
    repository?: string,
    commit?: string,
    build?: string,
    rawBookUrl?: string,
    stripHeadlinesAndFormatsFrom: IEbookType[],
}

interface lookAndFeel {
    extraCss: string,
    embedAllFonts: boolean,
    lineHeight: number,
    changeJustification: 'original' | 'left' | 'justified' 
}

interface txtInputOptions {
    formattingType: 'markdown'
}

interface mobiOutputOptions {
    mobiFileType: 'old' | 'both' | 'new'
}

interface epubOutputOptions {
        epubInlineToc: boolean,
        epubTocAtEnd: boolean,
}

interface azw3OutputOptions {
        prettyPrint: boolean
}

interface pdfOutputOptions {
    paperSize: string,
    pdfPageNumbers: boolean,
    prettyPrint: boolean,
    pdfDefaultFontSize: number,
    pdfAddToc: boolean,
    pdfHyphenate: boolean,
    pdfOddEvenOffset: number,
    pdfPageMarginTop: number,
    pdfPageMarginBottom: number,
    pdfPageMarginLeft: number,
    pdfPageMarginRight: number
}

type ITxtOutputFormatting = 'plain' | 'markdown' | 'textile';

interface txtOutputOptions {
    forceMaxLineLength: number,
    inlineToc: boolean,
    keepColor: boolean,
    keepImageReferences: boolean,
    keepLinks: boolean,
    prettyPrint: boolean,
    txtOutputFormatting: ITxtOutputFormatting,
}

interface rtfOutputOptions {
    prettyPrint: boolean
}

interface tableOfContents {
    tocTitle: string,
    level1Toc: '//h:h1' | '//h:h2' | '//h:h3',
    level2Toc: '//h:h2' | '//h:h3' | '//h:h4',
    level3Toc: '//h:h3' | '//h:h4' | '//h:h5',
}

interface markdownVariables {
    [key: string]: string
}

export type IBookProductionOptions = {
    general: IGeneralBookProductionOptions,
    metadata: metadata,
    additionalMetadata: additionalMetadata,
    lookAndFeel: lookAndFeel,
    txtInputOptions: txtInputOptions,
    mobiOutputOptions: mobiOutputOptions,
    epubOutputOptions: epubOutputOptions,
    azw3OutputOptions: azw3OutputOptions,
    pdfOutputOptions: pdfOutputOptions,
    txtOutputOptions: txtOutputOptions,
    rtfOutputOptions: rtfOutputOptions,
    tableOfContents: tableOfContents,
}

export type IEbookConvertOptions = {
    [key: string]: string | number | boolean
}

export type IBook = {
    bookName: string,
    bookTypes: IEbookType[],
    path: string
};
export type IEbookType = typeof VALID_EBOOK_TYPES[number];

export type IMetadata = metadata & additionalMetadata;
export type IParameter = { build: string, commit: string};
export type IMarkdownVariables = markdownVariables;

export type IFileDescriptor = {
    name: string,
    ext: string,
    url?: string 
} | null;


