import fs from 'fs';
import chalk from 'chalk';
import {FILE_TYPES} from '../common';
import {IFileDescriptor} from '../types';
import {GoogleCloudStorageFileIo} from '../Components/RemoteFileIo';
import {GoogleCloudSpeechApi} from '../Components/SpeechAi';
const MAX_PARALLEL_REQUESTS = 1;

const manualText = `
I did not find any audio files to transcribe. Currently only mp3 files are supported.
Please copy files that you want to transcribe into the directory:

./{{AUDIO_PATH}}/transcribe

When you call the 'ebook-maker transcribe' command again, they will be sent to the transcription service.
And after this moved to the audio directory:

./{{AUDIO_PATH}}

The transcribed text will be found as md file at 

./{{TEXT_PATH}}
`;

const FileIo = new GoogleCloudStorageFileIo({bucketName: 'ebook-maker'});

const transcribeAudioBook = async(language : string) : Promise < void > => {

    const speechApi = new GoogleCloudSpeechApi(language);
    const audioPath = `audio/${language}`;
    const textPath = `text/${language}`;
    const audioFiles = await readAudioFiles(language, `${process.cwd()}/${audioPath}/transcribe`);

    if (!audioFiles.length) {
        console.info(chalk.redBright(manualText
            .replace(/\{\{AUDIO_PATH\}\}/g, audioPath)
            .replace(/\{\{TEXT_PATH\}\}/g, textPath)
        ));
        return;
    }

    let audioFilesToProcess;
    while (audioFilesToProcess = audioFiles.splice(0, MAX_PARALLEL_REQUESTS)) {
        if (!audioFilesToProcess.length) break;

        await Promise.all(audioFilesToProcess.map(async(audioFile : IFileDescriptor) => {            
            const text = await speechApi.convertSpeechToText(audioFile);

            if (text) {
                await writeText(text, language, audioFile);
                moveAudioFile(language, audioFile);
            }
        }));
    }
}

const readAudioFiles = async(language : string, audioPath : string): Promise<IFileDescriptor[]> => {
    let audioFiles;
    try {
        audioFiles = fs
            .readdirSync(audioPath)
            .filter((file : string) => FILE_TYPES.find(fileType => file.endsWith(fileType)))
            .map((file : string) => {
                const re = new RegExp(`^(.*)\.(${FILE_TYPES.map(fileType => `(${fileType})`).join('|')})$`)
                const match = file.match(re);
                const fileName : IFileDescriptor = {
                    name: match && match[1] || '',
                    ext: match && match[2] || ''
                }

                return fileName.name
                    ? fileName
                    : null;
            })
            .reduce((acc : IFileDescriptor[], file : IFileDescriptor | null, _ : any, fileNames : IFileDescriptor[]) => {
                if (!file || fileNames.find((file2 : IFileDescriptor) => {
                    return file2 && file2.name === file.name && FILE_TYPES.indexOf(file2.ext) < FILE_TYPES.indexOf(file.ext)
                })) {
                    return acc;
                }

                if (!fs.existsSync(`${audioPath}/${file.name}.transcription`)) {
                    return [
                        ...acc,
                        file
                    ];
                }

                return acc;
            }, []);
    } catch (error) {
        if (error.code === 'ENOENT') {
            console.error(`I don't find audio for language '${language}' files at '${audioPath}'`);
            return [];
        }

        throw(error);
    }

    const files = audioFiles.length;
    files > 1 ? console.info(`I found ${audioFiles.length} audio files. Uploading them ...`) :
    files === 1 ? console.info(`I found 1 audio file. Uploading it ...`) :
    console.log('I didn´t find any audio files. Aborting ...');
    
    for (let audioFile of audioFiles) {
        if (!audioFile) 
            continue;
        
        const sourcePath = `${audioPath}/${audioFile.name}.${audioFile.ext}`;
        const {internalUrl} = await FileIo.upload(sourcePath);
        audioFile.url = internalUrl;

        console.info(`Uploaded '${audioFile.name}' to '${audioFile.url}'.`);
    }

    return audioFiles;
}

const writeText = async(text : string, language : string, audioFile : IFileDescriptor) => {
    if (!audioFile) return;

    const destPath = `${process.cwd()}/text/${language}/${audioFile.name}.md`;
    const mdText = textToMarkdown(audioFile.name, text);

    fs.writeFileSync(destPath, mdText);
}

const textToMarkdown = (title: string, text: string): string => {
    const templatePath = `${process.cwd()}/media/template.md`;
    const template = `${fs.readFileSync(templatePath)}`;
    return template
        .replace('${TITLE}', title)
        .replace('${TEXT}', text);
}

const moveAudioFile = (language : string, audioFile : IFileDescriptor) => {
    if (!audioFile) return;

    const srcPath = `${process.cwd()}/audio/${language}/transcribe/${audioFile.name}.${audioFile.ext}`;
    const destPath = `${process.cwd()}/audio/${language}/${audioFile.name}.${audioFile.ext}`;

    fs.renameSync(srcPath, destPath);
}

export default transcribeAudioBook;