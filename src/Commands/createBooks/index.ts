import {
    IBookProductionOptions,
    IEbookType,
    IParameter
} from '../../types';

import uploadBook, {getPublicPath} from './uploadBook';
import prepareBook from './prepareBook';
import convertBook from './convertBook';
import createMobi from './createMobi';

const createBooks = async(directory : string, config : IBookProductionOptions, language : string, bookTypes : IEbookType[], parameter : IParameter) => {
    const {
        lookAndFeel,
        txtInputOptions,
        pdfOutputOptions,
        epubOutputOptions,
        azw3OutputOptions,
        txtOutputOptions,
        rtfOutputOptions,
        tableOfContents,
        general: {
            bookTypesToUpload
        },
        additionalMetadata: {
            rawBookUrl
        }
    } = config;

    const bookName = `${config.metadata.title.replace(/[\s]/g, '')}_${language}`;
    if (!rawBookUrl?.startsWith('http')) config.additionalMetadata.rawBookUrl = bookTypesToUpload.map(bookType => `${getPublicPath()}/${bookName}.${bookType}`).join(', ');
    const srcFileNames = await prepareBook(directory, config, bookTypes, parameter);

    console.log(`Language: =  ${language}  = = = = = = = = = = = =`)

    for (const bookType of bookTypes) {
        const specificOptions = bookType === 'pdf' ? { ...pdfOutputOptions, ...tableOfContents }
            : bookType === 'azw3' ? { ...azw3OutputOptions, ...tableOfContents }
            : bookType === 'epub' ? { ...epubOutputOptions, ...tableOfContents }
            : bookType === 'txt' ? { ...txtOutputOptions }
            : bookType === 'rtf' ? { ...rtfOutputOptions }
            : {};

        const options = {
            language,
            ...config.metadata,
            ...lookAndFeel,
            ...txtInputOptions,
            ...specificOptions,
        }
    
        const bookPath = await convertBook(srcFileNames[bookType], bookName, options, bookType);
        if (bookTypesToUpload.includes(bookType)) await uploadBook(bookPath);

        if (bookType === 'epub') {
            const bookPath = await createMobi(bookName);
            if (bookTypesToUpload.includes(bookType)) await uploadBook(bookPath);
        }
    }
};

export default createBooks;