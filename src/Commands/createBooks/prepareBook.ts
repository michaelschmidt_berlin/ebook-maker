import fs from 'fs';
import YAML from 'yaml';
import chalk from 'chalk';
import {readChapters, replaceVariables, TEMP_DIR} from '../../common';

import {
    IBookProductionOptions,
    IMetadata,
    IMarkdownVariables,
    IEbookType,
    IParameter
} from '../../types';

type IFileData = {
    len: number,
    fileName: string,
    fileData: string
}

type ISourceFileNames = {
    [key : string] : string
};

const prepareFile = async(chapter : string, directory : string, metadata : IMetadata, bookType : IEbookType) : Promise < IFileData > => {
    const fileName = `${directory}/${chapter}`;
    const {stripHeadlinesAndFormatsFrom} = metadata;

    return new Promise((resolve, reject) => {
        fs.readFile(fileName, {}, async(err, fileDataBuffer : Buffer) => {
            let fileData = `${fileDataBuffer}`;
            if (err) 
                reject(err);
            else {
                fileData = resolveConditionals(fileData, bookType);
                fileData = resolveReferences(fileData, directory);
                fileData = resolveVariables(fileData, metadata);
                if (stripHeadlinesAndFormatsFrom.includes(bookType)) {
                    fileData = stripTitelsAndFormats(fileData);
                }
                resolve({len: fileData.length, fileName, fileData});
            }
        });
    })
}

const resolveReferences = (text : string, directory : string) => {
    const re = /(<div.*class=".*\b(infobox)\b.*".*id="(.*)".*>)</gm;
    let ref;
    const references = [];

    while ((ref = re.exec(text)) !== null) {
        references.push({replace: ref[1], type: ref[2], content: ref[3]})
    }

    for (let i = 0; i < references.length; i++) {
        const {replace, type, content} = references[i];

        const fileName = `${directory}/dictionary/${content}.yaml`;
        const yamlText = `${fs.readFileSync(fileName)}`;
        let referenceText;
        try {
            referenceText = YAML.parse(yamlText);
        } catch (error) {
            console.error(chalk.red(`Can't create YAML text from '${fileName}', because of '${error.message}'`));
            referenceText = {
                novel: 'There was an error reading this box.'
            }
        }

        switch (type) {
            case 'infobox':
                text = text.replace(replace, `${replace}${referenceText.novel}`);
        }
    }

    return text;
}

const resolveVariables = (text : string, metadata : IMetadata) : string => {
    // const projectRoot : string = findProjectRoot(process.cwd(), {maxDepth: 12})
    // || 'unknown';
    const projectRoot : string = process.cwd();

    const resolveData : IMarkdownVariables = {
        BOOK_VERSION: metadata.version,
        BITBUCKET_COMMIT: metadata.commit || 'snapshot',
        BITBUCKET_BUILD_NUMBER: metadata.build || 'x',
        BOOK_REPOSITORY: metadata.repository || '_',
        BOOK_LICENSE: metadata.license,
        BOOK_ASIN: metadata.asin,
        BOOK_ISBN: metadata.isbn,
        MEDIA_PATH: `${projectRoot}/media`,
        RAW_BOOK_URL: metadata.rawBookUrl || '',
    };

    return replaceVariables(text, resolveData);
}

const resolveConditionals = (text : string, bookType : IEbookType) : string => {
    let targetText : string = text;
    const re = /<!-- if booktype is ([\w,]+) -->(.*)<!-- endif booktype -->/g;
    let ref : RegExpExecArray | null;

    while ((ref = re.exec(text)) !== null) {
        const [completeText,
            bookTypes,
            replacementText] = ref;

        targetText = targetText.replace(completeText, bookTypes.includes(bookType)
            ? replacementText
            : '');
    }

    return targetText;
}

const stripTitelsAndFormats = (text : string) : string => {
    let targetText : string = text;
    const reStrip: RegExp[] = [
        /#{1,3}.*?\n/g, // Strip markdown headlines
        /<div.*class=\"impressum\".*?<\/div>/gs]; // strip impressum
    let ref : RegExpExecArray | null;

    reStrip.forEach(re => {
        while ((ref = re.exec(text)) !== null) {
            const [completeText] = ref;
    
            targetText = targetText.replace(completeText, '');
        }    
    })

    return targetText;
}

const prepareBook = async (directory : string, config : IBookProductionOptions, bookTypes : IEbookType[], parameter : IParameter): Promise<ISourceFileNames> => {
    const chapters = await readChapters(directory);
    const srcFileNames : ISourceFileNames = {};

    const metadata = {
        ...parameter,
        ...config.metadata,
        ...config.additionalMetadata
    }

    for (let bookType of bookTypes) {
        const bookData = await Promise.all(chapters.map(chapter => prepareFile(chapter, directory, metadata, bookType)));
        const bookText = bookData.reduce((acc : string, data : IFileData) => `${acc}\n${data.fileData}`, '');

        // ... convert book for every type TODO: change prepareFile to replace if/endif
        // structures

        srcFileNames[bookType] = `${TEMP_DIR}/ebookmaker_tmpfile.${bookType}.md`;

        fs.writeFileSync(srcFileNames[bookType], bookText);
    }

    return srcFileNames;
}

export default prepareBook;