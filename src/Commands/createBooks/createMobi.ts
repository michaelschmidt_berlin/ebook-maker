import chalk from 'chalk';
import {execSync} from 'child_process';
import {PROJECT_ROOT, PUBLIC_DIR} from '../../common';

const createMobi = (bookName : string) => {
    const epubPath = `${PROJECT_ROOT}/${PUBLIC_DIR}/${bookName}.epub`;

    // const cmd = `kindlegen --version`;
    const cmd = `kindlegen ${epubPath}`;

    console.log(`Converting epub to kindle format.`);

    try {
        execSync(cmd);
    } catch (err) {
        const stdout = `${err.stdout}`;
        if (!stdout.includes('Mobi file built')) {
            console.error(chalk.red(err.message));
            console.error(chalk.redBright(stdout));
            process.exit(1);
        }
    }

    return epubPath.replace('.epub', '.mobi');
};

export default createMobi;