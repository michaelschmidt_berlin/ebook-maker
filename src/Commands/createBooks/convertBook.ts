import {execSync} from 'child_process';
import fs from 'fs';
import chalk from 'chalk';
import {paramCase} from 'change-case';
import {IEbookConvertOptions, IEbookType} from '../../types';
import {PROJECT_ROOT, TEMP_DIR, PUBLIC_DIR} from '../../common';

const convertBook = (sourceFile : string, bookName : string, options : IEbookConvertOptions, type : IEbookType) => {
    const publicPath = `${PROJECT_ROOT}/${PUBLIC_DIR}`;

    if (!fs.existsSync(publicPath)) {
        fs.mkdirSync(publicPath);
    }

    const destPath = `${publicPath}/${bookName}.${type}`;

    const cmdOptions = Object
        .keys(options)
        .filter(option => options[option] !== null && options[option] !== false)
        .map(option => `--${paramCase(option)}${options[option] !== true
            ? ` "${options[option]}"`
            : ''}`)
        .join(' ');

    const cmd = `ebook-convert ${sourceFile} ${destPath} ${cmdOptions}`;

    console.time(type);
    console.log(`Converting markdown to ${type} format...`);

    let result;
    try {
        result = `${execSync(cmd, {cwd: TEMP_DIR})}`;
    } catch (err) {
        console.error(err);
        process.exit(1);
    }

    const potentialErrors = ['error', 'no start { of style declaration found']
    const errors = result.split('\n').filter(line => potentialErrors.reduce((acc : boolean, error : string) => acc || line.toLowerCase().includes(error), false)).map(line => line.trim());

    if (errors.length) {
        console.error(chalk.red(errors.join('\n')));
    }

    console.timeEnd(type);

    return destPath;
};

export default convertBook;
