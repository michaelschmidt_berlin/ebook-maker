import {GoogleCloudStorageFileIo} from '../../Components/RemoteFileIo';

const FileIo = new GoogleCloudStorageFileIo({bucketName: 'ebook-maker'});

const uploadBook = async (sourcePath: string): Promise<string> => {
    const {publicUrl} = await FileIo.upload(sourcePath);

    console.log(`Uploaded book to '${publicUrl}'`);

    return publicUrl;
}

export const getPublicPath = () : string => {
    return FileIo.publicPath();
}

export default uploadBook;
