const fs = require('fs');
const {
    readChapters
} = require('../common');

const defaultLanguages = ['de'];
const commander = require('commander');

commander
    .version('1.0.0')
    .option('-l, --languages [lang Code]', 'Specify one or more languages [de,en]')
    .parse(process.argv);

const languages = commander.languages && commander.languages.split(',') || defaultLanguages;

const analyseFile = async (chapter, language) => {
    const fileName = `${language}/${chapter}`;
    return new Promise((resolve, reject) => {
        fs.readFile(fileName, {}, async (err, fileData) => {
            if (err) reject(err);
            else {
                const text = `${fileData}`;
                const openingBrackets = text.match(/„/g) || [];
                const closingBrackets = text.match(/“/g) || [];
                const quotes = text.match(/„[\w\W]+?“/g) || [];

                if (openingBrackets.length === closingBrackets.length && openingBrackets.length === quotes.length) resolve(null);
                else resolve({
                    fileName,
                    openingBrackets: openingBrackets.length,
                    closingBrackets: closingBrackets.length,
                    quotesCount: quotes.length,
                    quotes
                });
            }
        });
    })
}

(async () => {
    for (language of languages) {
        const chapters = await readChapters(`${__dirname}/../${language}`);
        const results = await Promise.all(chapters.map(chapter => analyseFile(chapter, language)));
        const issues = results.filter(result => result);

        console.log(JSON.stringify(issues));
    }
})();
