require('dotenv').config();
const puppeteer = require('puppeteer');
const fs = require('fs');
const commander = require('commander');
const jqueryUrl = 'https://code.jquery.com/jquery-3.2.1.min.js';
const baseUrl = 'https://kdp.amazon.com/en_US/bookshelf';

class KDPScraper {    
    constructor(baseUrl) {
        this.baseUrl = baseUrl;
        this.auth = {
            name: process.env.KDP_ACCOUNT_NAME,
            pass: process.env.KDP_ACCOUNT_PASS,
        }
    }

    async prepareSite() {
        this.browser = await puppeteer.launch({
            headless: false
        });
        
        this.page = await this.browser.newPage();
        await this.page.setViewport({
            width: 1920,
            height: 1080,
            deviceScaleFactor: 1,
        });
        await this.page.goto(this.baseUrl, { timeout: 90000 });
        await this.page.addScriptTag({
            url: jqueryUrl
        });
        await this.page.$eval('#ap_email', (el, auth) => el.value = auth.name, this.auth);
        await this.page.$eval('#ap_password', (el, auth) => el.value = auth.pass, this.auth);
        await this.page.click('#signInSubmit');
        await this.page.waitFor('.mt-content');
    }

    async getBook(title) {
        return this.page.$$eval('.podbookshelftable tr.mt-row', (bookEntries, title) => {
            const bookEntry = bookEntries.filter(element => {
                const tooltips = element.querySelectorAll('[data-action="a-tooltip"]').values();
                let tooltip;

                do {
                    tooltip = tooltips.next();
                    const value = tooltip.value && tooltip.value.getAttribute('data-a-tooltip');

                    if (value && value.includes(title)) {
                        return true;
                    }
                } while (!tooltip.done);

                return false;
            });

            console.log(11.3, {bookEntry})
            return bookEntry.length && bookEntry[0].id || null;
        }, title);
    }

    async uploadBookData(bookEntryId) {
        await this.selectMenuOption(`#${bookEntryId}-actions`, 'Edit eBook Content');
        await this.page.waitFor(3000);
        await this.page.waitForSelector('.file-upload-browse-section button');
        const uploadButtons = await this.page.$$('.file-upload-browse-section button');
        const [fileChooser] = await Promise.all([
            this.page.waitForFileChooser(),
            uploadButtons[0].click(),
        ]);
        console.time('fileChooser');
        await fileChooser.accept([`${__dirname}/tmp/test.js`]);
        console.timeEnd('fileChooser');
        await this.page.waitFor(10000);

    }

    async selectMenuOption(selector, option) {
        const button = await this.page.$$eval(`${selector} a`, (buttons, option) => {
            const button = buttons.find(button => button.textContent.includes(option));

            return button.click();
        }, option);
    }
}

(async () => {
    commander
        .version('1.0.0')
        .option('-l, --languages [de]', 'Specify the languages, default: de')
        .option('-t, --types [mobi|pdf]', 'Specify the book types, default: mobi')
        .parse(process.argv);

    const bookTypes = commander.types && commander.types.split(',') || ['epub', 'mobi'];
    const languages = commander.languages && commander.languages.split(',') || ['de'];

    const scraper = new KDPScraper(baseUrl);
    await scraper.prepareSite();

    for (let bookType of bookTypes) {
        for (let language of languages) {
            const metadata = require(`../${language}/metadata`);
            const { title } = metadata.metadata;
            const bookEntryId = await scraper.getBook(title);
            await scraper.uploadBookData(bookEntryId);
        }
    }

    console.log(14.1)
})();
