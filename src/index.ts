#!/usr/bin/env node

import dotenv from 'dotenv';
dotenv.config();
import commander from 'commander';
import createBooks from './Commands/createBooks';
import normalizeBooks from './Commands/normalizeBooks';
import initProject from './Commands/initProject';
import transcribeAudioBook from './Commands/transcribeAudioBook';

import {IAction, IEbookType, IBookProductionOptions} from './types/index';
import  { VALID_EBOOK_TYPES } from './common';
const {version} = require('../package.json');

const create = async(projectRoot : string, config : IBookProductionOptions, language : string, bookTypes : IEbookType[], build : string, commit : string) => {
    console.assert(language && ['de', 'en', 'se'].includes(language), 'Please specify the language of the book (de, en or se)');
    bookTypes && bookTypes.forEach((type : IEbookType) => console.assert(VALID_EBOOK_TYPES.includes(type), `'${type}' is not a valid book type.`));

    const directory = `${projectRoot}/text/${language}`;
    await createBooks(directory, config, language, bookTypes, {build, commit});
}

const normalize = async(projectRoot : string, language : string) => {
    console.assert(language && ['de', 'en', 'se'].includes(language), 'Please specify the language of the book (de, en or se)');

    const directory = `${projectRoot}/text/${language}`;
    await normalizeBooks(directory);
}

const helpText = () => `
Version ${version}
Usage: ebook-maker [options] <action>

Actions:
  init                                  Initialize a project
  create                                Create ebooks
  transcribe                            Transcribe texts
  upload                                Upload texts
  normalize                             Normalize Texts

Options:
  -V, --version                         output the version number
  -l, --language [de | en | se]         Specify the language
  -t, --types [epub,pdf,mobi]           Specify the type of book, default: epub,mobi
  -b, --build [build number]            Pipeline build number
  -c, --commit [git commit]             Commit Id
  -a, --audioPath [/path/to/audio.wav]  Path to audio file (WAV)
  -p, --prepare                         Prepare for translating
  -s, --split                           Split text after translating
  -f, --inputFile                       File to be prepared or split
  -h, --help                            output usage information
`;


(async() => {
    commander
        .version(version)
        .arguments('<action>')
        .option('-l, --language [de | en | se]', 'Specify the language')
        .option('-t, --types [epub,pdf,mobi]', 'Specify the type of book, default: epub,mobi')

        // create
        .option('-b, --build [build number]', 'Pipeline build number')
        .option('-c, --commit [git commit]', 'Commit Id')

        // transcript
        .option('-a, --audioPath [/path/to/audio.wav]', 'Path to audio file (WAV)')

        // translate
        .option('-p, --prepare', 'Prepare for translating')
        .option('-s, --split', 'Split text after translating')
        .option('-f, --inputFile', 'File to be prepared or split')
        .parse(process.argv);

    if (process.argv.length < 3) {
        console.log(helpText());
        process.exit(0);
    }

    const {
        args,
        types = 'epub,mobi',
        build = 'snapshot',
        language = commander.language || process.env.DEFAULT_LANGUAGE || 'en',
        prepare,
        split,
        inputFile
    } = commander;

    const action = args[0]as IAction;

    if (action === 'init') {
        await initProject();

        process.exit();
    }

    const commit = commander.commit && commander
        .commit
        .substr(0, 7) || '_';
    // const projectRoot = findProjectRoot(process.cwd(), {maxDepth: 12}) || '';
    const projectRoot = process.cwd();

    const config = require(`${projectRoot}/config`);
    const bookTypes = types && types.split(',') || ['epub', 'mobi'];

    switch (action) {
        case 'create':
            await create(projectRoot, config, language, bookTypes, build, commit);
            break;

        case 'transcribe':
            const transcription = await transcribeAudioBook(language);
            break;

        case 'normalize':
            await normalize(projectRoot, language);
            break;

        default: 
            console.error(`I don't know the command '${action}'`);
            process.exit(1);
    }

    process.exit(0);
})();