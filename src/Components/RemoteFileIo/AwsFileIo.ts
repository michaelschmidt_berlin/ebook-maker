import AWS from 'aws-sdk';
import RemoteFileIo from '../RemoteFileIo';

type IAccessType = 'public-read';

interface IFileIoOptions {
    bucket: string,
    accessType?: IAccessType,
};

const accessKeyId = process.env.AWS_ACCESS_KEY_ID || '';
const secretAccessKey = process.env.AWS_SECRET_ACCESS_KEY || '';
const awsRegion = process.env.AWS_REGION || '';

export class AwsFileIo {
    S3: any = null;
    bucket: any = null;
    accessType: IAccessType = 'public-read';

    constructor(options: IFileIoOptions) {
        // super();

        console.assert(accessKeyId, 'Env variable AWS_ACCESS_KEY_ID missing.');
        console.assert(secretAccessKey, 'Env variable AWS_SECRET_ACCESS_KEY missing.');

        this.S3 = new AWS.S3({
            apiVersion: '2006-03-01',
            credentials: {
                accessKeyId,
                secretAccessKey
            }
        });

        this.bucket = options.bucket;
        this.accessType = options.accessType || 'public-read';
    }

    async upload(filePath: string, fileContent: string | Buffer): Promise<string> {
        return new Promise((resolve, reject) => {
            this.S3.upload({
                ACL: this.accessType,
                Body: fileContent,
                Bucket: this.bucket,
                Key: filePath
            }, (err: Error) => {
                if (err) reject(err);
                else {
                    const href = `https://${this.bucket}.s3.${awsRegion}.amazonaws.com/${filePath.replace(/\s/g, '+')}`;
                    resolve(href);
                }
            });    
        })
    }
}

export default AwsFileIo;