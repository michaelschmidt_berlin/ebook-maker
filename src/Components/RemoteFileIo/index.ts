import BaseComponent from '../BaseComponent';
export * from './AwsFileIo';
export * from './GoogleCloudStorageFileIo';

abstract class RemoteFileIo extends BaseComponent {
    constructor() {
        super();
    } 

    download(directory: string, fileName: string) {

    }

    upload(directory: string, fileName: string, fileData: string) {

    }
}

export default RemoteFileIo;
