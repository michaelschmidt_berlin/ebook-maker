import path from 'path';

import {Storage} from '@google-cloud/storage';
import {googleCredentialsWarning} from '../../common';

interface IFileIoOptions {
    bucketName: string,
};

interface IUploadReturn {
    publicUrl: string,
    internalUrl: string,
}

export class GoogleCloudStorageFileIo {
    storage: any = null;
    bucketName: string = '';

    constructor(options: IFileIoOptions) {
        // super();
        if (!process.env.GOOGLE_APPLICATION_CREDENTIALS) {
            throw(googleCredentialsWarning);
        }

        this.storage = new Storage();
        this.bucketName = options.bucketName;
    }

    publicPath(): string {
        return `https://storage.googleapis.com/${this.bucketName}`;
    }

    async upload(filePath: string, options?: any): Promise<IUploadReturn> {
        options = options || {};
        return new Promise(async (resolve, reject) => {
            const bucket = this.storage.bucket(this.bucketName);
            const fileName = path.basename(filePath);
            const file = bucket.file(fileName);

            await bucket.upload(filePath, options);
            await file.makePublic();

            resolve({
                internalUrl: `gs://${this.bucketName}/${fileName}`,
                publicUrl: `${this.publicPath()}/${fileName}`,
            });
        });
    }
}

export default GoogleCloudStorageFileIo;