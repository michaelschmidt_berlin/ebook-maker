import BaseComponent from '../BaseComponent';
export * from './GoogleCloudSpeechApi';

import { IFileDescriptor } from '../../types';

abstract class RemoteFileIo extends BaseComponent {
    constructor() {
        super();
    } 

    async convertSpeechToText(audioFile : IFileDescriptor, language : string) : Promise < string | null > {
        return '';
    }
}

export default RemoteFileIo;
