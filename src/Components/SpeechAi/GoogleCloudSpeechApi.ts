import {LANGUAGE_CODES, DEFAULT_SAMPLE_RATE_HERTZ, GOOGLE_TRANSCRIBE_CONFIDENCE_THRESHOLD} from '../../common';
import { google } from '@google-cloud/speech/build/protos/protos.d';
import { SpeechClient } from '@google-cloud/speech/build/src/v1p1beta1';
import { IFileDescriptor } from '../../types';
import { stdout as log } from 'single-line-log';
import {googleCredentialsWarning} from '../../common';

type IStream = {
    name: string,
    timeElapsed: number
}

type ITimeObject = {
    startTime: {
        seconds: string,
        nanos: number
    },
    lastUpdateTime: {
        seconds: string,
        nanos: number
    },
    uri: string
}

export class GoogleCloudSpeechApi {
    static streams: IStream[] = [];
    language: string = 'en';

    constructor(language: string) {
        if (!process.env.GOOGLE_APPLICATION_CREDENTIALS) {
            throw(googleCredentialsWarning);
        }

        this.language = language;
    }

    async convertSpeechToText(audioFile : IFileDescriptor) : Promise < string | null > {
        if (!audioFile) return null;

        const client = new SpeechClient();
    
        const encoding = 'MP3';
        const languageCode = LANGUAGE_CODES[this.language];
    
        const config: google.cloud.speech.v1p1beta1.IRecognitionConfig = {
            encoding,
            sampleRateHertz: DEFAULT_SAMPLE_RATE_HERTZ,
            languageCode
        };
    
        const audio = {
            uri: audioFile.url,
          };
      
        const request : google.cloud.speech.v1p1beta1.ILongRunningRecognizeRequest = {
            config: config,
            audio: audio
        };

        console.log(`Transcribing '${audioFile.name}' with language code '${languageCode}' ...`)

        const [operation] = await client.longRunningRecognize(request);
        operation.on('progress', (event: any) => GoogleCloudSpeechApi.logStatus(event, audioFile.name));
        const [response] = await operation.promise();

        GoogleCloudSpeechApi.clearStatus(audioFile.name);
    
        if (response) {
            const transcription = response
                .results!
                .map((result: google.cloud.speech.v1p1beta1.ISpeechRecognitionResult) => {
                    return result.alternatives!.map((alt) => {
                        const lowConfidence = alt.confidence && alt.confidence < GOOGLE_TRANSCRIBE_CONFIDENCE_THRESHOLD
                        return `${lowConfidence ? '(???)' : ''}${alt.transcript}`;
                    }).join('\n(Alternative:)')
                })
                .join('\n');
    
            return transcription;
        }
        
        return '';
    }

    static logStatus(event: ITimeObject, name: string) {
        const logEntry = GoogleCloudSpeechApi.streams.find(stream => stream.name === name);
        const timeElapsed: number = parseInt(event.lastUpdateTime.seconds) - parseInt(event.startTime.seconds);

        if (!logEntry) {
            GoogleCloudSpeechApi.streams.push({
                name,
                timeElapsed
            });
        } else {
            logEntry.timeElapsed = timeElapsed;
        }

        GoogleCloudSpeechApi.printStatus();
    }

    static clearStatus(name: string) {
        const index = GoogleCloudSpeechApi.streams.findIndex(stream => stream.name === name);
        if (index !== -1) GoogleCloudSpeechApi.streams.splice(index, 1);

        GoogleCloudSpeechApi.printStatus();
    }

    static printStatus() {
        console.log(GoogleCloudSpeechApi.streams.map(stream => `${stream.name}: ${stream.timeElapsed} sec.`).join('\n'))
    }
}

export default GoogleCloudSpeechApi;