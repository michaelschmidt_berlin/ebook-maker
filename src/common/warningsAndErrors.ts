import chalk from 'chalk';

export const googleCredentialsWarning = `
${chalk.red(`I'm missing the Google credentials file. You can get one at 'https://console.cloud.google.com'.
It looks like:`)}
${chalk.redBright(`
    {
        "type": "service_account",
        "project_id": "ebook-maker-111222333444555",
        "private_key_id": "122134234da324234234adaaddaddaad1",
        "private_key": "-----BEGIN PRIVATE KEY-----MII....wRA88Jtu5VH-----END PRIVATE KEY-----",
        "client_email": "starting-account-at45453ssaadd-@ebook-maker-3232424422424.iam.gserviceaccount.com",
        "client_id": "1111112222233333444444",
        "auth_uri": "https://accounts.google.com/o/oauth2/auth",
        "token_uri": "https://oauth2.googleapis.com/token",
        "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
        "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/..."
    }
`)}
${chalk.green(`And please enable following APIs:

    Cloud Speech-to-Text API
    Cloud Storage

And store the location of the credentials file in the GOOGLE_APPLICATION_CREDENTIALS env variable.
`)}`;
