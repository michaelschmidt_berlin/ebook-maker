import os from 'os';

export * from './readChapters';
export * from './replaceVariables';
export * from './convertSpeechToText';
export * from './warningsAndErrors';

export type IResolveData = { [key: string]: string; };

export const LANGUAGE_CODES: { [key: string]: string } = {
    de: 'de-DE',
    en: 'en-GB',
    sv: 'sv-SE'
} // For Google cloud language codes see https://cloud.google.com/speech-to-text/docs/languages?hl=de

export const VALID_EBOOK_TYPES = ['epub', 'pdf', 'azw3', 'txt', 'rtf'] as const;
export const FILE_TYPES: string[] = ['mp3'];
export const DEFAULT_SAMPLE_RATE_HERTZ = 44100;
export const GOOGLE_TRANSCRIBE_CONFIDENCE_THRESHOLD = 0.90; // Transcribed sentences below this confidence are marked

export const PROJECT_ROOT = process.cwd();
export const TEMP_DIR = os.tmpdir();
    // Maybe: const projectRoot = findProjectRoot(process.cwd(), {maxDepth: 12}) || '';
export const PUBLIC_DIR = 'public';
