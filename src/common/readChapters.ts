import fs from 'fs';

export const readChapters = async (directory: string): Promise<string[]> => {
    return new Promise((resolve, reject) => {
        fs.readdir(directory, (err, files) => {
            if (err) {
                reject(err);
                return;
            }

            files = files.filter(file => {
                return !!file.match(/^\d{2,4}/);
            }).sort();

            resolve(files);
        });
    });
}
