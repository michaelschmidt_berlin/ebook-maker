export const replaceVariables = (text: string, resolveData: { [key: string]: string; }): string => {
    let targetText: string = text;
    const re = /\$\{([^\}]+)\}/gm;
    let ref : RegExpExecArray | null;

    while ((ref = re.exec(text)) !== null) {
        targetText = targetText.replace(ref[0], resolveData[ref[1]]);
    }

    return targetText;
}
