import {LANGUAGE_CODES, DEFAULT_SAMPLE_RATE_HERTZ} from '../common';
import { google } from '@google-cloud/speech/build/protos/protos.d';
import { SpeechClient } from '@google-cloud/speech/build/src/v1p1beta1';
import { IFileDescriptor } from '../types';

export const convertSpeechToText = async(audioFile : IFileDescriptor, language : string) : Promise < string | null > => {
    if (!audioFile) return null;
    // Creates a client
    const client = new SpeechClient();

    const encoding = 'MP3';
    const languageCode = LANGUAGE_CODES[language];

    const config: google.cloud.speech.v1p1beta1.IRecognitionConfig = {
        encoding,
        sampleRateHertz: DEFAULT_SAMPLE_RATE_HERTZ,
        languageCode
        // sampleRateHertz: 44100,
        // languageCode: 'de-DE',
    };

    const audio = {
        uri: audioFile.url,
      };
  
    const request : google.cloud.speech.v1p1beta1.ILongRunningRecognizeRequest = {
        config: config,
        audio: audio
    };
    const [operation] = await client.longRunningRecognize(request);
    operation.on('progress', event => console.log(JSON.stringify(event)))
    const [response] = await operation.promise();

    if (response) {
        const transcription = response
            .results!
            .map((result: google.cloud.speech.v1p1beta1.ISpeechRecognitionResult) => result.alternatives![0].transcript)
            .join('\n');

        return transcription;
    }
    
    return '';
}

const logProgress = async () => {}